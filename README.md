# Charting for My Rails App

This project aims at building a charting component for My Rails App Reporting module.

## Pre-requisite

* Ruby 2.4.1
* Rails 5.0.3 + webpacker gem (v1.1)
* Vue.js 2.3+
* Node.js
* Yarn

## Developer Setup

Please make sure you have Ruby 2.4.1 and latest Node installed in your machine.

Check out source code and run the following commands to install dependencies.

```
git clone https://bitbucket.org/mindhub/tms-chart
bundle install
yarn install
```

To run the app, you need to run

```
rails server
```

and

```
./bin/webpack-dev-server
```

Lastly, open up a browser and navigate to `http://localhost:3000`

## Developing Vue.js-based chart

* The web page hosting Vue.js chart is under `app/views/charts/index.html.erb`
* The Vuejs code all start in `app/javascript/packs/application.js`
* webpack config is under `config/webpack/*` (See [webpacker](https://github.com/rails/webpacker) for further details.)

## Functional and Behavioral Specification of the Chart Component:

The user interface:

1)	Date Range: Pre-defined (1W, 2W, 1M, 3M, MTD, QTD, YTD, 1Y) or custom date range (from date / to date)

2)	KPI Metrics:
a.	Sales,
b.	Payment Received

The layout of the Vue.js chart component would be like below (Yes, copy from Stripe.com Dashboard). The top bar of this component contains pre-defined date range (on the left) and a custom date range selector (on the right). The next bar would contain the KPI metrics i.e. Sales and Payment Received. The rest of the chart would be the canvas for line chart.

3) RESTful API URL

All data would be provided with a RESTful API call to our Rails back-end API service. It could be defined as a prop in the given Vue.js component (i.e. configurable)

Behavior:

1) The component should be designed as mobile friendly. We use bootstrap 4 alpha5 version.
2) The custom date range should have drop down calendar to assist user to pick up date. All preset period should be clickable and refresh the line chart after clicked.
3) The KPI Metrics should
   a) Show the number for the given period (returned from RESTful API)
   b) be clickable - the line chart will then show the chart related to the selected metric. By default it would display the line chart for metric on the left first one.
