import Vue from 'vue/dist/vue.esm';
import LineChart from './charting/line_chart.vue';

document.addEventListener('DOMContentLoaded', () => {

  if (document.querySelector('#vue_chart')) {
    document.querySelector('#vue_chart').appendChild(document.createElement('hello'));
    const app = new Vue({
      el: 'hello',
      template: `<LineChart :fields="[{key: 'sales', value: 'https://demo.learnhop.com/api/reports?type=line-chart2&metric=sales'}, {key: 'payment', value: 'https://demo.learnhop.com/api/reports?type=line-chart2&metric=payments'}]"/>`,
      components: { LineChart }
    });
  }

});
