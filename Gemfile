source 'https://rubygems.org'

gem 'puma', '~> 3.0' # Use Puma as the app server
gem 'rails', '~> 5.0.2'
gem 'sqlite3'

# api and documentation generation
gem 'active_model_serializers' # json serializer

# rails asset pipelines
# gem 'coffee-rails', '~> 4.2' # Use CoffeeScript for .coffee assets and views
gem 'haml-rails'
gem 'sass-rails', '~> 5.0'    # Use SCSS for stylesheets
group :development, :production do
  gem 'uglifier', '>= 1.3.0'    # Use Uglifier as compressor for JavaScript assets
end
gem 'sprockets-es6' # compile es6

# Frontend components (See more in package.json)
gem 'webpacker', github: 'rails/webpacker', tag: 'v1.1.0' # freeze at v1.1 to avoid upgrade (prevent error)
gem 'jquery-rails' # Use jquery as the JavaScript library
gem "font-awesome-rails" #use font-awesome
gem 'foreman'

group :development do
  gem 'listen', '~> 3.0.5'
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
  gem 'web-console'
  # debugging
  gem "better_errors"
  gem "binding_of_caller"
  gem 'meta_request' # RailsPanel chrome extension
  # linting
	gem 'haml_lint', require: false
end

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug', platform: :mri
  # Testing frameworks
  gem 'factory_girl_rails'
  gem 'rspec-rails', '~> 3.5'
  gem 'spring-commands-rspec'
end

group :test do
  gem 'capybara'
  gem 'database_cleaner'
  gem 'guard'
  gem 'guard-livereload'
  gem 'guard-rspec'
  gem 'selenium-webdriver'
  gem 'shoulda-matchers'
  gem 'shoulda-callback-matchers'
  gem 'timecop'
  gem 'webmock'
end
